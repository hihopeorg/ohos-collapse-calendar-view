/**
 * The MIT License (MIT)
 *
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.wefika.calendar.example;

import com.wefika.calendar.CollapseCalendarView;
import com.wefika.calendar.widget.DayView;
import com.wefika.calendar.widget.WeekView;
import junit.framework.TestCase;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Image;
import ohos.app.Context;
import org.junit.Assert;

public class UITest extends TestCase {

    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;

    public void setUp() throws Exception {
        super.setUp();
        mContext = sAbilityDelegator.getAppContext();
    }

    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    public void testClick() {
        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1);
        CollapseCalendarView calendarView = (CollapseCalendarView) mainAbility.findComponentById(ResourceTable.Id_calendar);
        sleep(1);
        Image next = (Image) calendarView.findComponentById(ResourceTable.Id_next);
        Image prev = (Image) calendarView.findComponentById(ResourceTable.Id_prev);
        EventHelper.triggerClickEvent(mainAbility, next);
        sleep(1);
        EventHelper.triggerClickEvent(mainAbility, prev);
        sleep(1);
        WeekView weekView = (WeekView)(calendarView.getWeeksView().getComponentAt(0));
        DayView dayView = (DayView) weekView.getComponentAt(6);
        EventHelper.triggerClickEvent(mainAbility, dayView);
        sleep(3);
        Assert.assertFalse("It's OK.",dayView.isEnabled());

    }

    public void testMoveToWeek() {

        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(1);
        CollapseCalendarView calendarView = (CollapseCalendarView) mainAbility.findComponentById(ResourceTable.Id_calendar);
        sleep(1);
        // 模拟上滑
        EventHelper.inputSwipe(mainAbility, 533, 819, 533, 750, 100);
        sleep(1);
        int i = calendarView.getWeeksView().getChildCount();
        sleep(3);
        Assert.assertEquals(i,5);
        // 模拟下滑
        EventHelper.inputSwipe(mainAbility, 533, 319, 533, 650, 100);
        sleep(3);

    }


    private void sleep(int duration) {
        try {
            Thread.sleep(duration * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
