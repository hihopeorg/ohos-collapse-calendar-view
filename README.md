# ohos-collapse-calendar-view

**本项目是基于开源项目 collapse-calendar-view 进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/blazsolar/android-collapse-calendar-view ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ohos-collapse-calendar-view
- 所属系列：ohos的第三方组件适配移植
- 功能：日历月视图和周视图的切换UI效果
- 项目移植状态：完成
- 调用差异：无差异。
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/blazsolar/android-collapse-calendar-view
- 基线release版本：V0.1.0,SHA1:4348b1765aea6064787401e1d865a45979c55cb0
- 编程语言：Java
- 外部库依赖：无

#### 演示效果
![avatar](preview.gif)

#### 安装教程

##### 方案一：

1. 编译har包ohos-collapse-calendar-view_ohos.har。

2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：

  1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.wefika.calendar.ohos:ohos-collapse-calendar:1.0.0'
 }
```


#### 使用说明：
有关该项目的业务实现，请参见“entry”文件夹.

##### 第一步: 编写布局文件添加到你的布局文件中
```java
        ...
        <com.wefika.calendar.CollapseCalendarView
            ohos:id="$+id:calendar"
            ohos:width="match_parent"
            ohos:height="match_content"/>
        ...
```
##### 第二步: 初始化 CollapseCalendarView 
```java
    ......

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mCalendarView = (CollapseCalendarView) findComponentById(ResourceTable.Id_calendar);
        LocalDate selected = LocalDate.now();
        LocalDate minDate = LocalDate.now();
        LocalDate maxDate = LocalDate.now().plusYears(1);
        CalendarManager manager = new CalendarManager(selected, CalendarManager.State.MONTH, minDate, maxDate);
        mCalendarView.init(manager);
    }

    ......
```

#### 版本迭代

- v1.0.0

1. 目前支持功能如下：
   - 日历展示，日期选择
   - 月视图和周视图的切换

#### 许可信息

    The MIT License (MIT)
    
    Copyright (c) 2014 Blaž Šolar
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
